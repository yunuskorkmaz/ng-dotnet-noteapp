﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NoteApp.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Delete(TEntity entity);
        void Delete(Expression<Func<TEntity, bool>> where);
        TEntity Get(Expression<Func<TEntity, bool>> where);
        TEntity Get(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity>>[] include);
        TEntity Get(Expression<Func<TEntity, bool>> where, params string[] include);
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where = null);
        IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where, params string[] include);
        TEntity Insert(TEntity entity);
        void InsertOrUpdate(TEntity entity);
        void Update(TEntity entity);
        void Commit();
    }
}
