﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoteApp.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> GetRepo<TEntity>() where TEntity : class;
    }
}
