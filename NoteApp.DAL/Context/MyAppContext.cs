﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using NoteApp.DAL.Entities;

namespace NoteApp.DAL.Context
{
    public class MyAppContext : DbContext
    {
        public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
        {
            
        }

        public DbSet<Note> Note { get; set; }
        public DbSet<Folder> Folder { get; set; }

    }

   
}
