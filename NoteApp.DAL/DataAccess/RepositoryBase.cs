﻿using Microsoft.EntityFrameworkCore;
using NoteApp.DAL.Context;
using NoteApp.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NoteApp.DAL.DataAccess
{
    public class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly MyAppContext _dbContext;
        private readonly DbSet<TEntity> _dbset;

        public RepositoryBase(MyAppContext dbContext)
        {
            _dbContext = dbContext;
            _dbset = _dbContext.Set<TEntity>();
        }

        protected DbContext Context => _dbContext;

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<TEntity, bool>> where)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity>>[] include)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> where, params string[] include)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbset.ToList();
        }

        public IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where = null)
        {
           return (where == null ? _dbset : _dbset.Where(where));
        }

        public IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where, params string[] include)
        {
            var q = GetMany(where);
            for (int i = 0; i < include.Length; i++)
            {
                q = q.Include(include[i]);
            }
            return q;
        }

        public TEntity Insert(TEntity entity)
        {
            var insert = _dbset.Add(entity);
            Commit();
            return insert.Entity;
        }

        public void InsertOrUpdate(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
