﻿using System;
using System.Collections.Generic;
using System.Text;
using NoteApp.DAL.Context;
using NoteApp.DAL.Interfaces;

namespace NoteApp.DAL.DataAccess
{
    public class UnitOfWorkBase : IUnitOfWork
    {
        private MyAppContext context;
        private readonly IDictionary<Type, object> _repositories;
        public UnitOfWorkBase(MyAppContext _context)
        {
            context = _context;
            _repositories = new Dictionary<Type, object>();
        }
        public IRepository<TEntity> GetRepo<TEntity>() where TEntity : class
        {
            if (_repositories.Keys.Contains(typeof(TEntity)))
            {
                return _repositories[typeof(TEntity)] as IRepository<TEntity>;
            }
            var repository = new RepositoryBase<TEntity>(context);
            _repositories.Add(typeof(TEntity), repository);
            return repository;
        }
    }
}
