namespace NoteApp.DAL.Entities
{
    public class Folder
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}