﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoteApp.DAL.Entities
{
    public class Note
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int FolderID { get; set; }
        public string Tags { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdeteTime { get; set; }
    }
}
