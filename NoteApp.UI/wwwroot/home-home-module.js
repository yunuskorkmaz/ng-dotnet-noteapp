(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/ngx-markdown-editor/esm5/ngx-markdown-editor.js":
/*!**********************************************************************!*\
  !*** ./node_modules/ngx-markdown-editor/esm5/ngx-markdown-editor.js ***!
  \**********************************************************************/
/*! exports provided: LMarkdownEditorModule, MarkdownEditorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LMarkdownEditorModule", function() { return LMarkdownEditorModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarkdownEditorComponent", function() { return MarkdownEditorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");





var MarkdownEditorComponent = /** @class */ (function () {
    function MarkdownEditorComponent(required, maxlength, _renderer, _domSanitizer) {
        if (required === void 0) { required = false; }
        if (maxlength === void 0) { maxlength = -1; }
        this.required = required;
        this.maxlength = maxlength;
        this._renderer = _renderer;
        this._domSanitizer = _domSanitizer;
        this.hideToolbar = false;
        this.height = "300px";
        this._hideIcons = {};
        this.showPreviewPanel = true;
        this.isFullScreen = false;
        this._onChange = function (_) { };
        this._onTouched = function () { };
    }
    Object.defineProperty(MarkdownEditorComponent.prototype, "mode", {
        get: function () {
            return this._mode || 'editor';
        },
        set: function (value) {
            if (!value || (value.toLowerCase() !== 'editor' && value.toLowerCase() !== 'preview')) {
                value = 'editor';
            }
            this._mode = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MarkdownEditorComponent.prototype, "options", {
        get: function () {
            return this._options;
        },
        set: function (value) {
            var _this = this;
            this._options = value || {
                showBorder: true,
                hideIcons: []
            };
            this._hideIcons = {};
            (this._options.hideIcons || []).forEach(function (v) {
                _this._hideIcons[v] = true;
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MarkdownEditorComponent.prototype, "markdownValue", {
        get: function () {
            return this._markdownValue || '';
        },
        set: function (value) {
            var _this = this;
            this._markdownValue = value;
            this._onChange(value);
            if (this.preRender && this.preRender instanceof Function) {
                value = this.preRender(value);
            }
            if (value !== null && value !== undefined) {
                if (this._renderMarkTimeout)
                    clearTimeout(this._renderMarkTimeout);
                this._renderMarkTimeout = setTimeout(function () {
                    var html = marked(value || '', _this._markedOpt);
                    _this._previewHtml = _this._domSanitizer.bypassSecurityTrustHtml(html);
                }, 100);
            }
        },
        enumerable: true,
        configurable: true
    });
    MarkdownEditorComponent.prototype.ngOnInit = function () {
        var _markedRender = new marked.Renderer();
        _markedRender.code = function (code, language) {
            var validLang = !!(language && hljs.getLanguage(language));
            var highlighted = validLang ? hljs.highlight(language, code).value : code;
            return "<pre style=\"padding: 0; border-radius: 0;\"><code class=\"hljs " + language + "\">" + highlighted + "</code></pre>";
        };
        _markedRender.table = function (header, body) {
            return "<table class=\"table table-bordered\">\n<thead>\n" + header + "</thead>\n<tbody>\n" + body + "</tbody>\n</table>\n";
        };
        _markedRender.listitem = function (text) {
            if (/^\s*\[[x ]\]\s*/.test(text)) {
                text = text
                    .replace(/^\s*\[ \]\s*/, '<i class="fa fa-square-o" style="margin: 0 0.2em 0.25em -1.6em;"></i> ')
                    .replace(/^\s*\[x\]\s*/, '<i class="fa fa-check-square" style="margin: 0 0.2em 0.25em -1.6em;"></i> ');
                return "<li style=\"list-style: none;\">" + text + "</li>";
            }
            else {
                return "<li>" + text + "</li>";
            }
        };
        this._markedOpt = {
            renderer: _markedRender,
            highlight: function (code) { return hljs.highlightAuto(code).value; }
        };
    };
    MarkdownEditorComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var editorElement = this.aceEditorContainer.nativeElement;
        this.editor = ace.edit(editorElement);
        this.editor.$blockScrolling = Infinity;
        this.editor.getSession().setUseWrapMode(true);
        this.editor.getSession().setMode("ace/mode/markdown");
        this.editor.setValue(this.markdownValue || '');
        this.editor.on("change", function (e) {
            var val = _this.editor.getValue();
            _this.markdownValue = val;
        });
    };
    MarkdownEditorComponent.prototype.ngOnDestroy = function () {
    };
    MarkdownEditorComponent.prototype.writeValue = function (value) {
        var _this = this;
        setTimeout(function () {
            _this.markdownValue = value;
            if (typeof value !== 'undefined' && _this.editor) {
                _this.editor.setValue(value || '');
            }
        }, 1);
    };
    MarkdownEditorComponent.prototype.registerOnChange = function (fn) {
        this._onChange = fn;
    };
    MarkdownEditorComponent.prototype.registerOnTouched = function (fn) {
        this._onTouched = fn;
    };
    MarkdownEditorComponent.prototype.validate = function (c) {
        var result = null;
        if (this.required && this.markdownValue.length === 0) {
            result = { required: true };
        }
        if (this.maxlength > 0 && this.markdownValue.length > this.maxlength) {
            result = { maxlength: true };
        }
        return result;
    };
    MarkdownEditorComponent.prototype.insertContent = function (type) {
        if (!this.editor)
            return;
        var selectedText = this.editor.getSelectedText();
        var isSeleted = !!selectedText;
        var startSize = 2;
        var initText = '';
        var range = this.editor.selection.getRange();
        switch (type) {
            case 'Bold':
                initText = 'Bold Text';
                selectedText = "**" + (selectedText || initText) + "**";
                break;
            case 'Italic':
                initText = 'Italic Text';
                selectedText = "*" + (selectedText || initText) + "*";
                startSize = 1;
                break;
            case 'Heading':
                initText = 'Heading';
                selectedText = "# " + (selectedText || initText);
                break;
            case 'Refrence':
                initText = 'Refrence';
                selectedText = "> " + (selectedText || initText);
                break;
            case 'Link':
                selectedText = "[](http://)";
                startSize = 1;
                break;
            case 'Image':
                selectedText = "![](http://)";
                break;
            case 'Ul':
                selectedText = "- " + (selectedText || initText);
                break;
            case 'Ol':
                selectedText = "1. " + (selectedText || initText);
                startSize = 3;
                break;
            case 'Code':
                initText = 'Source Code';
                selectedText = "```language\r\n" + (selectedText || initText) + "\r\n```";
                startSize = 3;
                break;
        }
        this.editor.session.replace(range, selectedText);
        if (!isSeleted) {
            range.start.column += startSize;
            range.end.column = range.start.column + initText.length;
            this.editor.selection.setRange(range);
        }
        this.editor.focus();
    };
    MarkdownEditorComponent.prototype.togglePreview = function () {
        this.showPreviewPanel = !this.showPreviewPanel;
        this.editorResize();
    };
    MarkdownEditorComponent.prototype.previewPanelClick = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    MarkdownEditorComponent.prototype.fullScreen = function () {
        this.isFullScreen = !this.isFullScreen;
        this._renderer.setElementStyle(document.body, 'overflowY', this.isFullScreen ? 'hidden' : 'auto');
        this.editorResize();
    };
    MarkdownEditorComponent.prototype.editorResize = function (timeOut) {
        var _this = this;
        if (timeOut === void 0) { timeOut = 100; }
        if (this.editor) {
            setTimeout(function () {
                _this.editor.resize();
                _this.editor.focus();
            }, timeOut);
        }
    };
    return MarkdownEditorComponent;
}());
MarkdownEditorComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'md-editor',
                styles: [".md-editor-container{margin-bottom:15px;border:1px solid rgba(0,0,0,.1)}.md-editor-container.fullscreen{margin:0;position:fixed;border:0;top:0;left:0;width:100%;height:100%;z-index:99999999}.md-editor-container.fullscreen .editor-panel,.md-editor-container.fullscreen .preview-panel{height:calc(100vh - 40px)!important}.md-editor-container .ace-editor{height:100%}.md-editor-container .tool-bar{background-color:#f5f5f5;border-bottom:1px solid rgba(0,0,0,.1)}.md-editor-container .tool-bar .btn-group{padding:6px}.md-editor-container .tool-bar .btn-group>.btn:first-child::before{content:' ';background-color:#a9a9a9;width:1px;height:24px;left:-9px;top:2px;position:absolute}.md-editor-container .tool-bar .btn-group.hide-split>.btn:first-child::before,.md-editor-container .tool-bar .btn-group:first-child>.btn:first-child::before{display:none}.md-editor-container .tool-bar .btn{margin-bottom:0}.md-editor-container .editor-container{display:-webkit-box;display:-ms-flexbox;display:flex}.md-editor-container .editor-container>div{-webkit-box-flex:1;-ms-flex:1;flex:1}.md-editor-container .preview-panel{border-left:1px solid rgba(0,0,0,.1);background-color:#fff;padding:10px;overflow-y:auto}.md-editor-container .md-footer{padding:2px;background-color:#f0f0f0;font-size:12px;border-top:1px solid rgba(0,0,0,.1)}"],
                template: "<div class=\"md-editor-container\" [class.fullscreen]=\"isFullScreen\">\n  <div class=\"tool-bar\" *ngIf=\"!hideToolbar && mode != 'preview'\">\n    <div class=\"btn-group\">\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Bold\" (click)=\"insertContent('Bold')\" *ngIf=\"!_hideIcons.Bold\">\n        <i class=\"fa fa-bold\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Italic\" (click)=\"insertContent('Italic')\" *ngIf=\"!_hideIcons.Italic\">\n        <i class=\"fa fa-italic\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Heading\" (click)=\"insertContent('Heading')\" *ngIf=\"!_hideIcons.Heading\">\n        <i class=\"fa fa-header\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Refrence\" (click)=\"insertContent('Refrence')\" *ngIf=\"!_hideIcons.Refrence\">\n        <i class=\"fa fa-quote-left\"></i>\n      </button>\n    </div>\n    <div class=\"btn-group\">\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Link\" (click)=\"insertContent('Link')\" *ngIf=\"!_hideIcons.Link\">\n        <i class=\"fa fa-link\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Image\" (click)=\"insertContent('Image')\" *ngIf=\"!_hideIcons.Image\">\n        <i class=\"fa fa-image\"></i>\n      </button>\n    </div>\n    <div class=\"btn-group\">\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Unordered List\" (click)=\"insertContent('Ul')\" *ngIf=\"!_hideIcons.Ul\">\n        <i class=\"fa fa-list-ul\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Ordered List\" (click)=\"insertContent('Ol')\" *ngIf=\"!_hideIcons.Ol\">\n        <i class=\"fa fa-list-ol\"></i>\n      </button>\n      <button class=\"btn btn-sm btn-default\" type=\"button\" title=\"Code Block\" (click)=\"insertContent('Code')\" *ngIf=\"!_hideIcons.Code\">\n        <i class=\"fa fa-file-code-o\"></i>\n      </button>\n    </div>\n    <div class=\"btn-group\">\n      <button class=\"btn btn-sm btn-default\" type=\"button\" [attr.title]=\"showPreviewPanel ? 'Hide Preview' : 'Show Preview'\" (click)=\"togglePreview()\"\n        *ngIf=\"!_hideIcons.TogglePreview\">\n        <i class=\"fa\" [class.fa-eye]=\"!showPreviewPanel\" [class.fa-eye-slash]=\"showPreviewPanel\"></i>\n      </button>\n    </div>\n    <div class=\"btn-group pull-right hide-split\">\n      <button class=\"btn btn-sm btn-default\" type=\"button\" [class.active]=\"isFullScreen\" (click)=\"fullScreen()\" *ngIf=\"!_hideIcons.FullScreen\">\n        <i class=\"fa fa-arrows-alt\"></i>\n      </button>\n    </div>\n  </div>\n  <div class=\"editor-container\">\n    <div [style.display]=\"mode == 'preview' ? 'none' : null\">\n      <div class=\"editor-panel\" [style.height]=\"height\">\n        <div class=\"ace-editor\" #aceEditor></div>\n      </div>\n    </div>\n    <div [style.display]=\"showPreviewPanel ? 'block' : 'none'\" (click)=\"previewPanelClick($event)\">\n      <div class=\"preview-panel\" [innerHtml]=\"_previewHtml\" [style.height]=\"height\"></div>\n    </div>\n  </div>\n  <div *ngIf=\"maxlength > 0 && mode != 'preview'\">\n    <div class=\"text-right md-footer\">\n      {{ markdownValue?.length }} / {{ maxlength }}\n    </div>\n  </div>\n</div>\n",
                providers: [
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return MarkdownEditorComponent; }),
                        multi: true
                    },
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return MarkdownEditorComponent; }),
                        multi: true
                    }
                ]
            },] },
];
MarkdownEditorComponent.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"], args: ['required',] },] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"], args: ['maxlength',] },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], },
]; };
MarkdownEditorComponent.propDecorators = {
    "aceEditorContainer": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['aceEditor',] },],
    "hideToolbar": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    "height": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    "preRender": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    "mode": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    "options": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var LMarkdownEditorModule = /** @class */ (function () {
    function LMarkdownEditorModule() {
    }
    return LMarkdownEditorModule;
}());
LMarkdownEditorModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [
                    MarkdownEditorComponent
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"]
                ],
                exports: [
                    MarkdownEditorComponent
                ]
            },] },
];
LMarkdownEditorModule.ctorParameters = function () { return []; };


//# sourceMappingURL=ngx-markdown-editor.js.map


/***/ }),

/***/ "./src/app/home/home-index/home-index.component.html":
/*!***********************************************************!*\
  !*** ./src/app/home/home-index/home-index.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n        <app-note-list (ShowPanel)=\"ChangeDisplay($event)\" [Notes]=\"Notes\" ></app-note-list>\r\n\r\n        <app-note-detail (CreateOrUpdateEvent)=\"loadData($event)\" [DisplayPanel]=\"DisplayPanel\" [Note]=\"selectedNote\"\r\n        [FolderId]=\"FolderId\"></app-note-detail>\r\n\r\n"

/***/ }),

/***/ "./src/app/home/home-index/home-index.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/home/home-index/home-index.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-note-detail {\n  width: 100%; }\n\napp-note-list {\n  width: 25%;\n  min-width: 200px; }\n"

/***/ }),

/***/ "./src/app/home/home-index/home-index.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/home/home-index/home-index.component.ts ***!
  \*********************************************************/
/*! exports provided: HomeIndexComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeIndexComponent", function() { return HomeIndexComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeIndexComponent = /** @class */ (function () {
    function HomeIndexComponent(route, router, http) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.http = http;
        this.Notes = [];
        this._notes = [];
        this.DisplayPanel = "Empty";
        this.router.events.subscribe(function (val) {
            _this.ChangeDisplay({ type: 'Empty', note: _this.selectedNote });
            _this.Notes = _this._notes.filter(function (a) { return a.FolderID == _this.FolderId; });
        });
    }
    Object.defineProperty(HomeIndexComponent.prototype, "FolderId", {
        get: function () {
            var _this = this;
            this.route.params.subscribe(function (params) {
                _this._FolderId = +params['id'];
                return _this._FolderId;
            });
            return this._FolderId;
        },
        set: function (value) {
            this._FolderId = value;
        },
        enumerable: true,
        configurable: true
    });
    HomeIndexComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    HomeIndexComponent.prototype.ChangeDisplay = function (p) {
        this.DisplayPanel = p.type;
        this.selectedNote = p.note != null ? p.note : this.selectedNote;
    };
    HomeIndexComponent.prototype.loadData = function () {
        var _this = this;
        this.http.get("api/Note/Get/").subscribe(function (values) {
            _this._notes = values.json();
            _this.Notes = _this._notes.filter(function (a) { return a.FolderID == _this.FolderId; });
        });
    };
    HomeIndexComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-index',
            template: __webpack_require__(/*! ./home-index.component.html */ "./src/app/home/home-index/home-index.component.html"),
            styles: [__webpack_require__(/*! ./home-index.component.scss */ "./src/app/home/home-index/home-index.component.scss")],
            providers: []
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], HomeIndexComponent);
    return HomeIndexComponent;
}());



/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_index_home_index_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home-index/home-index.component */ "./src/app/home/home-index/home-index.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: ':id',
        component: _home_index_home_index_component__WEBPACK_IMPORTED_MODULE_2__["HomeIndexComponent"]
    },
    {
        path: '',
        component: _home_index_home_index_component__WEBPACK_IMPORTED_MODULE_2__["HomeIndexComponent"]
    }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_markdown_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-markdown-editor */ "./node_modules/ngx-markdown-editor/esm5/ngx-markdown-editor.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _home_index_home_index_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-index/home-index.component */ "./src/app/home/home-index/home-index.component.ts");
/* harmony import */ var _maincomponents_page_header_page_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../maincomponents/page-header/page-header.component */ "./src/app/maincomponents/page-header/page-header.component.ts");
/* harmony import */ var _note_list_note_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./note-list/note-list.component */ "./src/app/home/note-list/note-list.component.ts");
/* harmony import */ var _note_detail_note_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./note-detail/note-detail.component */ "./src/app/home/note-detail/note-detail.component.ts");
/* harmony import */ var _maincomponents_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../maincomponents/navbar/navbar.component */ "./src/app/maincomponents/navbar/navbar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeRoutingModule"],
                ngx_markdown_editor__WEBPACK_IMPORTED_MODULE_4__["LMarkdownEditorModule"]
            ],
            declarations: [
                _home_index_home_index_component__WEBPACK_IMPORTED_MODULE_6__["HomeIndexComponent"],
                _maincomponents_page_header_page_header_component__WEBPACK_IMPORTED_MODULE_7__["PageHeaderComponent"],
                _note_list_note_list_component__WEBPACK_IMPORTED_MODULE_8__["NoteListComponent"],
                _note_detail_note_detail_component__WEBPACK_IMPORTED_MODULE_9__["NoteDetailComponent"],
                _maincomponents_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__["NavbarComponent"]
            ],
            providers: []
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/home/note-detail/note-detail.component.html":
/*!*************************************************************!*\
  !*** ./src/app/home/note-detail/note-detail.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"note-detail\">\r\n\r\n  <nav class=\"navbar navbar-light\">\r\n      <form class=\"form-inline\">\r\n\r\n          <button class=\"btn btn-sm btn-outline-secondary\" type=\"button\">Kaydet</button>\r\n\r\n      </form>\r\n  </nav>\r\n\r\n  <div *ngIf=\"DisplayPanel == 'Add'\">\r\n\r\n  <input [(ngModel)]=\"InsertNote.Title\" type=\"text\">\r\n\r\n  <md-editor name=\"Content\" [(ngModel)]=\"InsertNote.Content\" [height]=\"height\" [mode]=\"mode\"></md-editor>\r\n\r\n    <button type=\"button\" (click)=\"AddNote()\">Ekle</button>\r\n  </div>\r\n\r\n  <div *ngIf=\"DisplayPanel == 'Detail'\">\r\n  <input [(ngModel)]=\"Note.Title\" type=\"text\">\r\n  <md-editor name=\"Content\" [(ngModel)]=\"Note.Content\" [height]=\"height\" [mode]=\"mode\"></md-editor>\r\n\r\n  </div>\r\n\r\n  <div *ngIf=\"DisplayPanel == 'Empty'\">\r\n    Boş\r\n  </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/home/note-detail/note-detail.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/home/note-detail/note-detail.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".note-detail {\n  display: flex;\n  flex-direction: column;\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/home/note-detail/note-detail.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/home/note-detail/note-detail.component.ts ***!
  \***********************************************************/
/*! exports provided: NoteDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoteDetailComponent", function() { return NoteDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NoteDetailComponent = /** @class */ (function () {
    function NoteDetailComponent(http, route) {
        this.http = http;
        this.route = route;
        this.CreateOrUpdateEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.InsertNote = {
            Content: "",
            Title: "",
            FolderID: this.FolderId
        };
    }
    Object.defineProperty(NoteDetailComponent.prototype, "mode", {
        get: function () {
            if (this.DisplayPanel == "Add") {
                return "editor";
                console.log("editor");
            }
            else if (this.DisplayPanel == "Detail")
                return "preview";
            else
                return "editor";
        },
        set: function (value) {
            this._mode = value;
        },
        enumerable: true,
        configurable: true
    });
    NoteDetailComponent.prototype.AddNote = function () {
        var _this = this;
        this.InsertNote.FolderID = this.FolderId;
        console.log(this.InsertNote);
        this.http.post("api/Note/Add", this.InsertNote).subscribe(function (result) {
            _this.CreateOrUpdateEvent.emit("Add");
            _this.InsertNote.Content = "";
            _this.InsertNote.Title = "";
            _this.DisplayPanel = "Detail";
            _this.mode = "preview";
            _this.Note = result.json();
            console.log(result.json());
        });
    };
    NoteDetailComponent.prototype.ngOnInit = function () {
        this.height = (window.innerHeight - (200)) + "px";
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], NoteDetailComponent.prototype, "DisplayPanel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NoteDetailComponent.prototype, "Note", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NoteDetailComponent.prototype, "FolderId", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NoteDetailComponent.prototype, "CreateOrUpdateEvent", void 0);
    NoteDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-note-detail',
            template: __webpack_require__(/*! ./note-detail.component.html */ "./src/app/home/note-detail/note-detail.component.html"),
            styles: [__webpack_require__(/*! ./note-detail.component.scss */ "./src/app/home/note-detail/note-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], NoteDetailComponent);
    return NoteDetailComponent;
}());



/***/ }),

/***/ "./src/app/home/note-list/note-list.component.html":
/*!*********************************************************!*\
  !*** ./src/app/home/note-list/note-list.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"notelist\">\r\n<div class=\"header\">\r\n    <div class=\"header-title\">\r\n        <button></button>\r\n\r\n        <span>Notes</span>\r\n    \r\n        <button class=\"new-note\" type=\"button\" (click)=\"showNotePanel({type : 'Add'})\">\r\n          <i class=\"fa fa-plus\" ></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"header-search\">\r\n      <div class=\"search-box\">\r\n          <input type=\"text\" >\r\n          <button><i class=\"fa fa-search\"></i></button>\r\n      </div>\r\n    </div>\r\n    \r\n\r\n</div>\r\n  \r\n  \r\n<div class=\"list-group\">\r\n  <div *ngFor=\"let note of Notes\" (click)=\"showNotePanel({type : 'Detail', note : note})\"  class=\"list-group-item\">\r\n    <div>\r\n      <h5>{{ note.Title }}</h5>\r\n      <p>\r\n        {{ note.Content }}\r\n      </p>\r\n      <small>{{ note.CreateDate }}</small>\r\n    </div>\r\n    \r\n  </div>\r\n  \r\n</div></div>\r\n"

/***/ }),

/***/ "./src/app/home/note-list/note-list.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/home/note-list/note-list.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".notelist {\n  display: flex;\n  flex-direction: column;\n  background-color: #fafafa;\n  height: 100%;\n  overflow: hidden; }\n  .notelist .header {\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    border-bottom: 1px solid #ddd; }\n  .notelist .header-title {\n      display: flex;\n      width: 100%;\n      justify-content: space-between;\n      padding: var(--gutter); }\n  .notelist .header-search {\n      padding: 10px var(--gutter); }\n  .notelist .header-search .search-box {\n        border: 1px solid #ddd;\n        border-radius: 3px;\n        padding: 4px 10px;\n        display: flex;\n        flex-direction: row; }\n  .notelist .header-search .search-box input {\n          width: 100%; }\n  .notelist .header-search .search-box input,\n        .notelist .header-search .search-box button {\n          border: none;\n          background: transparent;\n          outline: none; }\n  .notelist .header-search .search-box button {\n          margin-left: 5px;\n          padding: 3px 5px; }\n  .notelist .list-group {\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    overflow-y: auto;\n    padding: 5px 0; }\n  .notelist .list-group-item {\n      cursor: pointer;\n      padding: var(--gutter);\n      border-bottom: 1px solid #ddd; }\n  .notelist .list-group-item:hover {\n        background-color: #90a4ae;\n        color: #eceff1; }\n  .notelist .list-group-item h5 {\n        font-size: 1.1em;\n        font-weight: bold;\n        padding-bottom: 5px; }\n  .notelist .list-group-item p {\n        display: -webkit-box;\n        max-height: 2.8em;\n        margin: 0 auto;\n        font-size: 1em;\n        line-height: 1.4;\n        line-clamp: 2;\n        box-orient: vertical;\n        overflow: hidden;\n        text-overflow: ellipsis; }\n"

/***/ }),

/***/ "./src/app/home/note-list/note-list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/note-list/note-list.component.ts ***!
  \*******************************************************/
/*! exports provided: NoteListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoteListComponent", function() { return NoteListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NoteListComponent = /** @class */ (function () {
    function NoteListComponent() {
        this.ShowPanel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    NoteListComponent.prototype.ngOnInit = function () {
    };
    NoteListComponent.prototype.showNotePanel = function (type) {
        this.ShowPanel.emit(type);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NoteListComponent.prototype, "ShowPanel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], NoteListComponent.prototype, "Notes", void 0);
    NoteListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-note-list',
            template: __webpack_require__(/*! ./note-list.component.html */ "./src/app/home/note-list/note-list.component.html"),
            styles: [__webpack_require__(/*! ./note-list.component.scss */ "./src/app/home/note-list/note-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NoteListComponent);
    return NoteListComponent;
}());



/***/ }),

/***/ "./src/app/maincomponents/navbar/navbar.component.css":
/*!************************************************************!*\
  !*** ./src/app/maincomponents/navbar/navbar.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.navbar-brand {\r\n  padding-top: .75rem;\r\n  padding-bottom: .75rem;\r\n  font-size: 1rem;\r\n  background-color: rgba(0, 0, 0, .25);\r\n  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);\r\n}\r\n\r\n.navbar .form-control {\r\n  padding: .75rem 1rem;\r\n  border-width: 0;\r\n  border-radius: 0;\r\n}\r\n\r\n.form-control-dark {\r\n  color: #fff;\r\n  background-color: rgba(255, 255, 255, .1);\r\n  border-color: rgba(255, 255, 255, .1);\r\n}\r\n\r\n.form-control-dark:focus {\r\n    border-color: transparent;\r\n    box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);\r\n  }\r\n"

/***/ }),

/***/ "./src/app/maincomponents/navbar/navbar.component.html":
/*!*************************************************************!*\
  !*** ./src/app/maincomponents/navbar/navbar.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar bg-dark flex-md-nowrap p-0 shadow\">\r\n  <ul class=\"navbar-nav px-3\">\r\n    <li class=\"nav-item text-nowrap\">\r\n      <a class=\"nav-link\" href=\"#\">Sign out</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/maincomponents/navbar/navbar.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/maincomponents/navbar/navbar.component.ts ***!
  \***********************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/maincomponents/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/maincomponents/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/maincomponents/page-header/page-header.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/maincomponents/page-header/page-header.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/maincomponents/page-header/page-header.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/maincomponents/page-header/page-header.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom\">\r\n    <h1 class=\"h2\">{{  headerText }}</h1>\r\n    <div class=\"btn-toolbar mb-2 mb-md-0\">\r\n        <ng-content></ng-content>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/maincomponents/page-header/page-header.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/maincomponents/page-header/page-header.component.ts ***!
  \*********************************************************************/
/*! exports provided: PageHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageHeaderComponent", function() { return PageHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageHeaderComponent = /** @class */ (function () {
    function PageHeaderComponent() {
        this.headerText = "Page";
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], PageHeaderComponent.prototype, "headerText", void 0);
    PageHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-header',
            template: __webpack_require__(/*! ./page-header.component.html */ "./src/app/maincomponents/page-header/page-header.component.html"),
            styles: [__webpack_require__(/*! ./page-header.component.css */ "./src/app/maincomponents/page-header/page-header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PageHeaderComponent);
    return PageHeaderComponent;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map