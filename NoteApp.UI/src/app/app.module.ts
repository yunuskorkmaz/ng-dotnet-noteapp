import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router"
import { HttpModule } from "@angular/http"
import { AppComponent } from './app.component';
import { SidebarComponent } from './maincomponents/sidebar/sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
    {
      path : '',
      redirectTo: 'home/0',
      pathMatch : 'full'
    },
    {
      path: 'home/:id',
      loadChildren : './home/home.module#HomeModule'
    }
];


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
