import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Http } from "@angular/http";
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
   
})
export class SidebarComponent implements OnInit {
    closeResult: string;
    public FolderList: Folder[] = [];

    constructor(
        private modalService: NgbModal,
        private http : Http,
        private router : Router
        ) 
    { 
        this.http.get("api/Folder/GetAll").subscribe(data => {
            
            this.FolderList = data.json() as Folder[];
            this.router.navigate(['home', this.FolderList[0].ID]);
        })
    }

    open(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addEventForUI(){
     
    }

    save(newFolder){
        var folder : Folder = {Name : newFolder}
        this.http.post("api/Folder/Add",folder).subscribe(data => {
            this.FolderList.push(data.json() as Folder); 
        });
        this.modalService.dismissAll();
    }


    ngOnInit() {
    }

}


export interface Folder {
    ID?: number,
    Name: string
}