export interface Note {
    ID?: number;
    Title?: string;
    Content?: string;
    FolderID?: number;
    Tags?: string | any;

    CreateTime?: Date | string;

    UpdeteTime?: Date | string;
}