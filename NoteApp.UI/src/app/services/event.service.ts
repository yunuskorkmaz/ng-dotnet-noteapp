import { Injectable, Output, EventEmitter} from '@angular/core';
import { Subject } from 'rxjs';



@Injectable()
export class EventService {
    public EventHandle : EventEmitter<string>;

    constructor() {
        this.EventHandle = new EventEmitter<string>();
        
    }

    public AddEvent(name :string){
       
        this.EventHandle.emit(name);
       
    }
}