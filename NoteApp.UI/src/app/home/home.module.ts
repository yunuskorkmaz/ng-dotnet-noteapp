import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http"
import { FormsModule } from "@angular/forms"

import { LMarkdownEditorModule } from 'ngx-markdown-editor';

import { HomeRoutingModule } from './home-routing.module';
import { HomeIndexComponent } from './home-index/home-index.component';
import { PageHeaderComponent } from '../maincomponents/page-header/page-header.component';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteDetailComponent } from './note-detail/note-detail.component';
import { NavbarComponent } from '../maincomponents/navbar/navbar.component';

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        HomeRoutingModule,
        LMarkdownEditorModule
    ],
    declarations: [
        HomeIndexComponent,
        PageHeaderComponent,
        NoteListComponent,
        NoteDetailComponent,
        NavbarComponent
    ],
    providers :[
        
    ]
})
export class HomeModule { }
