import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MarkdownEditorComponent } from 'ngx-markdown-editor';
import { Http } from '@angular/http';
import { Note } from '../../models/note';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-note-detail',
    templateUrl: './note-detail.component.html',
    styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {
    @Input() DisplayPanel: string;
    @Input() public Note? : Note;
    @Input() FolderId : any;
    @Output() CreateOrUpdateEvent = new EventEmitter();

    public InsertNote : Note = {
        Content : "",
        Title : "",
        FolderID : this.FolderId
    };
   
    private _mode: string;
    public get mode(): string {
        if(this.DisplayPanel == "Add")
           {
                return "editor"; 
                console.log("editor");
           } 
        else if (this.DisplayPanel == "Detail")
            return "preview";
        else
            return "editor";
    }
    public set mode(value: string) {
        this._mode = value;
    }

    public height : any;

   
    constructor(private http : Http , private route : ActivatedRoute) {   
    }

    AddNote(){
        this.InsertNote.FolderID = this.FolderId;
        console.log(this.InsertNote);
        this.http.post("api/Note/Add", this.InsertNote).subscribe(result => {
            this.CreateOrUpdateEvent.emit("Add");
            this.InsertNote.Content ="";
            this.InsertNote.Title = "";
            this.DisplayPanel ="Detail";
            this.mode ="preview";
            this.Note = result.json() as Note;
            console.log(result.json());
            
        })
    }

    ngOnInit() {
        this.height = (window.innerHeight - (200)) + "px";      
    }
}
