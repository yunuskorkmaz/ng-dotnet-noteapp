import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Note } from '../../models/note';

@Component({
    selector: 'app-note-list',
    templateUrl: './note-list.component.html',
    styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit {
    @Output() ShowPanel = new EventEmitter();
    @Input() Notes : Note[];
    constructor() { }

    ngOnInit() {
    }
    showNotePanel(type) {
        this.ShowPanel.emit(type);
        
    }
}