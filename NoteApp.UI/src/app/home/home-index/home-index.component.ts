import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http"
import { ActivatedRoute,Router } from '@angular/router';
import { Note } from '../../models/note';

@Component({
    selector: 'app-home-index',
    templateUrl: './home-index.component.html',
    styleUrls: ['./home-index.component.scss'],
    providers:[]
})
export class HomeIndexComponent implements OnInit {
    

    private _FolderId: number;
    public get FolderId(): number {
        this.route.params.subscribe(params => {
             this._FolderId = +params['id'];
             return this._FolderId;
        });
        return this._FolderId;
        
    }
    public set FolderId(value: number) {
        this._FolderId = value;
    }
    public Notes: Note[] = [];
    public _notes: Note[] = [];
    public DisplayPanel: string = "Empty";
    public selectedNote?: Note;
    
    constructor(
        private route: ActivatedRoute,
        private router : Router,
        private http : Http
        ) 
        {
            
            this.router.events.subscribe(val => {
                this.ChangeDisplay({ type: 'Empty' ,note:this.selectedNote as Note });
                this.Notes = this._notes.filter(a => a.FolderID == this.FolderId);
            });
           

        }

    ngOnInit() {
        this.loadData();
    }
    ChangeDisplay(p) { 
        this.DisplayPanel = p.type;
        this.selectedNote = (p.note as Note) != null ? p.note : this.selectedNote;
    }

    loadData() {
        this.http.get("api/Note/Get/").subscribe(values => {
            this._notes = values.json() as Note[];    
            this.Notes = this._notes.filter(a => a.FolderID == this.FolderId);  
        });
    }

}
