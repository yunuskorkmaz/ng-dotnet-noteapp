using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NoteApp.DAL.Entities;
using NoteApp.DAL.Interfaces;

namespace NoteApp.UI.Controllers
{
    [Route("api/[controller]")]
    public class NoteController : Controller
    {
        private readonly IUnitOfWork db;

        public NoteController(IUnitOfWork _db)
        {
            db = _db;    
        }

        [HttpPost("[action]")]
        public Note Add([FromBody] Note note){
            var repo = db.GetRepo<Note>();
            note.CreateTime = DateTime.Now;
            var result = repo.Insert(note);
            return result;
        }

        [HttpGet("[action]")]
        public IEnumerable<Note> Get()
        {
            var repo = db.GetRepo<Note>();
            return repo.GetAll();
        }
    }
}
