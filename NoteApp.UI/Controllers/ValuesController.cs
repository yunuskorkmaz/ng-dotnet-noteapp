using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NoteApp.DAL.Entities;
using NoteApp.DAL.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NoteApp.UI.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IUnitOfWork db;
        // GET: api/<controller>

        public ValuesController(IUnitOfWork _db)
        {
            db = _db;
        }
        [HttpGet]
        public IEnumerable<Note> Index()
        {
            var repo = db.GetRepo<Note>();
            repo.Insert(new Note()
            {
                ID = 1,
                Title = "note1"

            });
            var list = db.GetRepo<Note>().GetAll();
            return list;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
