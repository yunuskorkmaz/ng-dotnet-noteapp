using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NoteApp.DAL.Entities;
using NoteApp.DAL.Interfaces;

namespace NoteApp.UI.Controllers
{
   [Route("api/[controller]")]
    public class FolderController : Controller
    {
        // GET: api/<controller>
        private readonly IUnitOfWork db;
        public FolderController(IUnitOfWork _db)
        {
            db = _db;
        }
        [HttpPost("[action]")]
        public Folder Add([FromBody]Folder folder)
        {
            var repo = db.GetRepo<Folder>();
            return repo.Insert(folder);
        }

        [HttpGet("[action]")]
        public IEnumerable<Folder> GetAll()
        {
            var repo = db.GetRepo<Folder>();
            return repo.GetAll();
        }
    }
}