var gulp = require('gulp');
var sass = require('gulp-sass');


gulp.task('css', function () {

    return gulp.src('./src/scss/styles.scss')
        .pipe(sass())
        .pipe(gulp.dest('./wwwroot/'))

});

gulp.task('default', ['css']);
